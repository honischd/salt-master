xorg:
  pkg:
    - installed

feh:
  pkg:
    - installed

{% if grains['os'] == 'Ubuntu' %}
rdesktop:
  pkg:
    - installed

scrot:
  pkg:
    - installed

indicator-sound-switcher:
  pkgrepo.managed:
    - name: deb http://ppa.launchpad.net/yktooo/ppa/ubuntu trusty main
    - dist: trusty
    - file: /etc/apt/sources.list.d/yktooo-ppa-trusty.list
    - key: 0x66efe2d582ce5d8a
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: indicator-sound-switcher
  pkg.latest:
    - name: indicator-sound-switcher
    - refresh: True

network-manager-gnome:
  pkg:
    - installed

xfce4-power-manager:
  pkg:
    - installed
{% endif %}

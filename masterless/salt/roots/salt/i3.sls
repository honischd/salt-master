{% for user, args in pillar['users'].iteritems() %}
i3:
  pkg:
    - installed
    - require:
      - pkg: xorg

/home/{{ user }}/.i3:
  file.symlink:
    - target: /home/{{ user }}/config/i3
    - user: {{ user }}
    - require:
      - git: config

/home/{{ user }}/images/background.jpg:
  file.managed:
    - source: salt://images/background.jpg
    - user: {{ user }}
    - group: {{ user }}
    - makedirs: True
    - require:
      - pkg: i3
{% endfor %}

base:
  '*':
    {% if "usecase" in grains and grains['usecase'] == 'server' %}
    - users
    - base_packages
    - git
    - vim
    - config
    {% elif grains['os'] == 'MacOS' %}
    - users
    - groups
    - base_packages
    - git
    - vim
    - emacs
    - config
    {% else %}
    - users
    - groups
    - base_packages
    - more_packages
    - git
    - vim
    - emacs
    - config
    - i3
    {% endif%}

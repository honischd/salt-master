{% for user, args in pillar['users'].iteritems() %}
{{ user }}:
{% if 'gid' in args %}
  group.present:
    - gid: {{ args['gid'] }}
{% endif %}
  user.present:
    - home: {{ args['home'] }}
    - shell: {{ args['shell'] }}
{% if 'uid' in args %}
    - uid: {{ args['uid'] }}
{% endif %}
{% if 'gid' in args %}
    - gid: {{ args['gid'] }}
{% endif %}
{% if 'password' in args %}
    - password: {{ args['password'] }}
{% if 'enforce_password' in args %}
    - enforce_password: {{ args['enforce_password'] }}
{% endif %}
{% endif %}
    - fullname: {{ args['fullname'] }}
{% if 'groups' in args %}
    - groups: {{ args['groups'] }}
{% endif %}
{% if 'key.pub' in args and args['key.pub'] == True %}
{{ user }}_key.pub:
  ssh_auth:
    - present
    - user: {{ user }}
    - source: salt://users/key.pub
{% endif %}
generate_key_{{ user }}:
  cmd.run:
    - creates: {{ salt['user.info'](user).get('home') }}/.ssh/id_rsa
    - name: ssh-keygen -q -N '{{ user }}' -f /{{ salt['user.info'](user).get('home') }}/.ssh/id_rsa
    - user: {{ user }}
{% endfor %}

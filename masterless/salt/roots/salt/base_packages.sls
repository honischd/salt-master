{% if not grains['os'] == 'MacOS' %}
build-essential:
  pkg:
    - installed
{% endif %}

python:
  pkg:
    - installed

python3:
  pkg:
    - installed

ansible:
  pkg:
    - installed

zsh:
  pkg:
    - installed

tmux:
  pkg:
    - installed

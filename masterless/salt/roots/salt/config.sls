{% for user, args in pillar['users'].iteritems() %}
config:
  git.latest:
    - name: https://gitlab.com/halderm/config.git
    - rev: master
    - target: {{ salt['user.info'](user).get('home') }}/config
    - user: {{ user }}
    - require:
      - pkg: git
      {% if grains['os'] == 'MacOS' %}
      - pkg: vim
      {% else %}
      - pkg: vim-nox
      - pkg: python-dev
      {% endif %}
      - pkg: cmake

ohmyzsh:
  git.latest:
    - name: https://github.com/robbyrussell/oh-my-zsh.git
    - rev: master
    - user: {{ user }}
    - target: {{ salt['user.info'](user).get('home') }}/src/oh-my-zsh
    - require:
      - pkg: git
      - pkg: zsh

cliist:
  git.latest:
    - name: https://github.com/ddksr/cliist.git
    - rev: master
    - user: {{ user }}
    - target: {{ salt['user.info'](user).get('home') }}/src/cliist
    - require:
      - pkg: python3

{{ salt['user.info'](user).get('home') }}/.oh-my-zsh:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/src/oh-my-zsh
    - user: {{ user }}
    - require:
      - git: ohmyzsh

{{ salt['user.info'](user).get('home') }}/.zshrc:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/zsh/zshrc
    - user: {{ user }}
    - require:
      - git: ohmyzsh
      - git: config

{{ salt['user.info'](user).get('home') }}/src/oh-my-zsh/custom/mhalder.zsh:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/zsh/custom/mhalder.zsh
    - require:
      - git: ohmyzsh
      - git: config

{{ salt['user.info'](user).get('home') }}/src/oh-my-zsh/themes/mhalder.zsh-theme:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/zsh/themes/mhalder.zsh-theme
    - require:
      - git: ohmyzsh
      - git: config

{% if not grains['os'] == 'MacOS' %}
{{ salt['user.info'](user).get('home') }}/.bashrc:
  file.prepend:
    - text:
      - TERM=xterm-256color
{% endif %}

{{ salt['user.info'](user).get('home') }}/.gitconfig:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/git/gitconfig
    - user: {{ user }}
    - require:
      - git: config

{{ salt['user.info'](user).get('home') }}/.githelpers:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/git/githelpers
    - user: {{ user }}
    - require:
      - git: config

{{ salt['user.info'](user).get('home') }}/.gitignore:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/git/gitignore
    - user: {{ user }}
    - require:
      - git: config

{{ salt['user.info'](user).get('home') }}/.gvimrc:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/gvimrc
    - user: {{ user }}
    - require:
      - git: config

{{ salt['user.info'](user).get('home') }}/.vimrc:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/vimrc
    - user: {{ user }}
    - require:
      - git: config

{{ salt['user.info'](user).get('home') }}/.vim:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/vim
    - user: {{ user }}
    - require:
      - git: config

{{ salt['user.info'](user).get('home') }}/.tmux.conf:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/tmux.conf
    - user: {{ user }}
    - require:
      - git: config
      - pkg: tmux

{{ salt['user.info'](user).get('home') }}/bin:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/bin
    - user: {{ user }}
    - require:
      - git: config
{% endfor %}
